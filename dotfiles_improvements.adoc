= dotfiles improvements for Ubuntu 18.04 with Ansible
Christoffer Holmstedt
v0.1, 2018-04-27

== Basics
. install vim, git and ansible if not already installed
. clone git@gitlab.com:christofferholmstedt/dotfiles.git for:
.. git global settings
.. Keyboard mappings in autostart (no more caps lock)...nice to have left altGr
there but rarely uses it. (does not seem to work in Ubuntu 18.04)
.. .vimrc settings with vim-powerline plugin

=== GNOME 3
==== Privacy and tracking issues
[listing]
gsettings set org.gnome.desktop.privacy report-technical-problems false
gsettings set org.gnome.desktop.privacy send-software-usage-stats false
gsettings set org.gnome.desktop.privacy remember-app-usage false
gsettings set org.gnome.desktop.privacy recent-files-max-age 1
gsettings set org.gnome.desktop.privacy remember-recent-files false

Manually go in to settings in the upper right corner and disable connectivity
checker.

==== Nautilus
[listing]
gsettings set org.gtk.Settings.FileChooser show-hidden true
gsettings set org.gnome.nautilus.list-view default-zoom-level small
gsettings set org.gnome.nautilus.preferences default-folder-viewer list-view
gsettings set org.gnome.nautilus.preferences always-use-location-entry true

==== Date time format setting
For basic improved settings without the plugin below, start with these settings
at least.

[listing]
gsettings set org.gnome.desktop.calendar show-weekdate true
gsettings set org.gnome.desktop.interface clock-show-date true
gsettings set org.gnome.desktop.interface clock-show-weekday true
gsettings set org.gnome.desktop.interface enable-animations false

----
sudo apt-get install nodejs
git clone -o daniel-khodabkhsh https://github.com/Daniel-Khodabakhsh/datetime-format.git
cd datetime-format
git checkout release
node --user_strict build.js
gnome-shell-extension-tool -e datetime-format@Daniel-Khodabakhsh.github.com
----

install "gnome-tweak-tool" and start !gnome-shell-extension-prefs" to set
format: %Y-%m-%dT%H:%M:%S%z

== Firefox settings
. show blank start page
. disable "check your spelling as you type"
. Downloads -> Always ask you where to save files
. Set DuckDuckGo as default search engine
. Disable "Provide search suggestions" and "Show search suggestions in address
bar results"
. Disable "Remember logins and passwords for websites"
. Configure "Firefox will _never remember history"
. Disable address bar suggestions from "Browsing history", "Bookmarks" and
"Open tabs"
. Tracking protection -> Always
. Block all sites from requesting access to my location
. Block all sites from requesting access to my camera
. Block all sites from requesting access to my microphone
. Block all sites from requesting access to my "Notifications"
. Disable " firefox to send technical and interaction data to Mozilla"
. Install HTTPS Everywhere https://www.eff.org/https-everywhere
. Install AdBlock Plus https://addons.mozilla.org/en-US/firefox/addon/adblock-plus/?src=search
.. Enable "Block additional tracking"
.. Enable "Block social media icons tracking"
.. Disable "acceptable ads"
. Install Asciidoctor.js Live Preview plugin https://addons.mozilla.org/en-US/firefox/addon/asciidoctorjs-live-preview/
. Install LeechBlock NG https://addons.mozilla.org/en-US/firefox/addon/leechblock-ng/
.. Configure following times: 0000-1600,1830-2400
.. Configure "blank page"
.. Configure following sites to be blocked:

[listing]
*aftonbladet.se
*cnn.com
*dn.se
*expressen.se
*facebook.com
*instagram.com
*linkedin.com
*news.ycombinator.com
*phoronix.com
*svt.se
*sweclockers.com
*swedroid.se
*youtube.com

== Inkscape development
. Enable sources (Inkscape is in Universe) in /etc/apt/sources.list
. $ sudo apt-get update
. $ sudo apt-get build-dep inkscape
. $ sudo apt-get install libsoup2.4-dev libgtk-3-dev libgtkmm-3.0-dev libgdl-3-dev

== Android development
. Download latest Android Studio
. unzip to ~/opt
. start and set android sdk location to ~/.androidsdk instead of default
~/Android

== Java development (Eclipse)

== Virtualbox

